import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  card1: CardModel;
  card2: CardModel;
  card3: CardModel;
  card4: CardModel;

  constructor() { }

  setCardDataValues() {
    this.card1 = {
      title: 'Churn Rate',
      icon: 'fas fa-chart-bar',
      iconType: 'bg-danger',
      statistics: {
        value: 2500,
        percentageIncOrDecValue: 3.5,
        percentageIncOrDecTime: 'Since last week'
      }
    };
    this.card2 = {
      title: 'Total Number of Customers',
      icon: 'fas fa-chart-pie',
      iconType: 'bg-info',
      statistics: {
        value: 50000,
        percentageIncOrDecValue: 3.5,
        percentageIncOrDecTime: 'Since yesterday'
      }
    };
    this.card3 = {
      title: 'Number of Churned Customers',
      icon: 'fas fa-users',
      iconType: 'bg-yellow',
      statistics: {
        value: 52000,
        percentageIncOrDecValue: 13.5,
        percentageIncOrDecTime: 'Since last year'
      }
    };
    this.card4 = {
      title: 'Number of New Customers',
      icon: 'fas fa-percent',
      iconType: 'bg-warning',
      statistics: {
        value: 2000,
        percentageIncOrDecValue: 1.5,
        percentageIncOrDecTime: 'Since yesterday'
      }
    };
  }
  ngOnInit() {
    this.setCardDataValues();

  }

}
