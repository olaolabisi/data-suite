interface CardModel {
  title: string;
  iconType: string;
  icon: string;
  statistics: CardStatistics;
}

interface CardStatistics {
  value: number;
  percentageIncOrDecValue: number;
  percentageIncOrDecTime: string;
}
