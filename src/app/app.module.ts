import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './Core-components/navbar/navbar.component';
import { FooterComponent } from './Core-components/footer/footer.component';
import { LandingPageComponent } from './Featured-Components/landing-page/landing-page.component';
import { CardListComponent } from './Featured-Components/card-list/card-list.component';
import { CardComponent } from './Featured-Components/card-list/card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LandingPageComponent,
    CardListComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
